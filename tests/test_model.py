import os
import os.path
import unittest

import model 

class ModelTestSuite(unittest.TestCase):

    def setUp(self):
        model.Model.set_db_name("test.db")
        self._cleanup()
        model.Model.reset(prime_with_test_data=True)
        self.model = model.Model()
    def tearDown(self):
        try :
            self.model.close_ds()
            self.model=None
            self._cleanup()
        except PermissionError:
            # If a test fails, the cleanup on tearDown tends to fail
            # as well because the file is still open.
            # Let this go, the next setUp should deal with the problem.
            pass
        
    def test_list_rdv_ids(self) :
        id_list = self.model.list_rdv_ids()
        self.assertEqual(1,len(id_list))

    def test_get_rdv_details(self) :
        ( rdv_id, ) = self.model.list_rdv_ids()
        rdv = self.model.get_rdv_details(rdv_id)
        self.assertEqual(2,len(rdv.participants))
        self.assertEqual(2,len(rdv.participants[0].position_reports))
        self.assertEqual(1,len(rdv.participants[1].position_reports))

    # Methods intended for private use only
    def _cleanup(self) :
        try :
            os.remove(model._db_name)
        except FileNotFoundError :
            pass    

if __name__ == '__main__':
    unittest.main()

import logging
import os
import os.path
import re
import sys
import threading
import time
import unittest

import flask
from selenium import webdriver

import views
import model

_TEST_DB_NAME = "test.db"

# Snippet from
# https://github.com/sdpython/ensae_teaching_cs/blob/master/src/ensae_teaching_cs/td_1a/flask_helper.py
# which enables the flask server to run in a thread in parallel with client test methods
class FlaskInThread (threading.Thread):
    def __init__(self, app):
        threading.Thread.__init__(self)
        self.daemon = True
        self._app = app
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.WARNING)

    def run(self):
        self._app.run()

    def shutdown(self):
        # It is not possible to shutdown the flask server from outside
        # Instead we implement a request to the server which causes
        # it to shut itself down
        # raise NotImplementedError()
        pass
        
class WebAppTestSuite(unittest.TestCase):

    def setUp(self):
        model.Model.set_db_name(_TEST_DB_NAME)
        self._cleanup()
        model.Model.reset(prime_with_test_data=True)
        self.model = model.Model()
        self.url_base="http://127.0.0.1:5000"
        FlaskInThread(views.app).start()
        #self._create_firefox_driver()
        
    def tearDown(self):
        self._open_chrome_driver()
        self.driver.get(self.url_base+"/shutdown")
        self._close_driver()
        try :
            self._cleanup()
        except PermissionError:
            # If a test fails, the cleanup on tearDown tends to fail
            # as well because the file is still open.
            # Let this go, the next setUp should deal with the problem.
            pass
        
    # The next two test-private functions are used
    # to pause for a convenient time after each screen
    # is displayed and before the test is completed.
    # The pauses are for witness convenience, they are 
    # not intended to affect the outcome of the test.
    def _pause_after_display(self):
        time.sleep(2.0)
    def _pause_at_end(self):
        time.sleep(5.0)
        
    # The next function does something similar to the 
    # two before but is intended for use when the test
    # actually requires a wait
    def _pause_for_change(self,seconds):
        time.sleep(seconds) 
        
    def test_rdv(self):
        # The default page is displayed, the first user presses the button 
        # to get started
        self._open_chrome_driver()
        self.driver.get(self.url_base)
        get_started_button=self.driver.find_element_by_id("get_started")
        get_started_button.click()
        self._pause_after_display()      
        # The RDV creation page is displayed, the first user fills in 
        # his name and a name for the rendezvous, then presses the button
        # to create it.
        user_name_field=self.driver.find_element_by_id("user_name")
        rdv_name_field=self.driver.find_element_by_id("rdv_name")
        localtime_field=self.driver.find_element_by_id("meet_dt")
        submit_button=self.driver.find_element_by_id("submit")
        user_name_field.value="Test User"
        rdv_name_field.value="Meet up for testing"
        localtime_field.click()
        self._pause_after_display()      
        submit_button.click()
        # The map is displayed
        url=self.driver.current_url
        url_param_string=url.replace(self.url_base+"/rendezvous?","")
        url_param_dict={}
        for key_and_value in url_param_string.split('&') :
            (key,value) = key_and_value.split('=')
            url_param_dict[key]=value
        self.assertIn('rdv_id', url_param_dict.keys())
        #self.assertNotIn('ptcpt_id',url_param_dict.keys())
        self._pause_at_end()
        self._close_driver()
    
    # Methods intended for private use only
    def _cleanup(self) :
        try :
            os.remove(_TEST_DB_NAME)
        # If the :memory: database is used, it isn't a surprise
        # that trying to delete it from the filesystem fails.
        except OSError: pass
        except FileNotFoundError: pass    

    def _open_firefox_driver(self):
        fp = webdriver.FirefoxProfile()
        fp.set_preference("browser.startup.homepage", "about:blank")
        fp.set_preference("startup.homepage_welcome_url", "about:blank")
        fp.set_preference("startup.homepage_welcome_url.additional", "about:blank")
        fp.set_preference("browser.startup.homepage_override.mstone", "ignore")
        self.driver = webdriver.Firefox(fp)

    def _open_chrome_driver(self):
        self.driver = webdriver.Chrome(
            os.path.join(os.environ["VIRTUAL_ENV"],"Scripts","chromedriver.exe")
        )
    
    def _close_driver(self):
        self.driver.close()
        self.driver = None
        
import os
import os.path
import unittest

from datastore import *

_TEST_DB_NAME = 'test.db'

class DataStoreTestSuite(unittest.TestCase):

    def setUp(self):
        self._cleanup()
    def tearDown(self):
        try :
            self._cleanup()
        except PermissionError:
            # If a test fails, the cleanup on tearDown tends to fail
            # as well because the file is still open.
            # Let this go, the next setUp should deal with the problem.
            pass
        
    # Tests associated with create_db
    def test_create_db(self):
        self.assertFalse(os.path.exists(_TEST_DB_NAME))
        DataStore.create_db(_TEST_DB_NAME)
        self.assertTrue(os.path.exists(_TEST_DB_NAME))
    def test_cant_create_same_db_twice(self):
        DataStore.create_db(_TEST_DB_NAME)
        with self.assertRaises(DatabaseAlreadyExistsError) as context :
            DataStore.create_db(_TEST_DB_NAME)
        
    # Tests associated with open_db
    def test_open_and_close_db(self):
        DataStore.create_db(_TEST_DB_NAME)
        ds = DataStore(_TEST_DB_NAME)
        ds.open_db()
        ds.close_db()
        ds = None
    def disabled_test_cant_open_nonexistent_db(self):
        with self.assertRaises(DatabaseDoesNotExistError) as context :
            ds = DataStore(_TEST_DB_NAME)        
            ds.open_db()
            ds.close_db()
            ds = None
    def test_empty_schema(self):
        RDV_TEST_SQL = "select * from RENDEZVOUS"
        PTCPT_TEST_SQL = "select * from PARTICIPANT"
        substitution_map = { }
        DataStore.create_db(_TEST_DB_NAME)
        datastore = DataStore(_TEST_DB_NAME)
        rdv_rows,rdv_column_info = datastore._select(RDV_TEST_SQL,substitution_map)
        self.assertEqual(0,len(rdv_rows))
        self.assertEqual(RDV_NUM_COLUMNS,len(rdv_column_info))
        ptcpt_rows,ptcpt_column_info = datastore._select(PTCPT_TEST_SQL, substitution_map)
        self.assertEqual(0,len(ptcpt_rows))
        self.assertEqual(PTCPT_NUM_COLUMNS,len(ptcpt_column_info))
        
    # Tests associated with the main operations
    def test_create_rdv(self):
        DUMMY_NAME="Shopping"
        datastore = DataStore.create_db(_TEST_DB_NAME)
        test_rdv = model_entities._test_rdv_object()
        test_rdv.name = DUMMY_NAME
        (rdv_id,ptcpt1_id,ptcpt2_id)=datastore._create_test_data([test_rdv])
        TEST_SELECT_BY_ID_SQL = "select * from RENDEZVOUS where id = :id"
        rdv_id8 = datastore._id_base64_to_long(rdv_id)
        ( the_only_row ) , _ = datastore._select(TEST_SELECT_BY_ID_SQL,{'id':rdv_id8})
        self.assertEqual(1,len(the_only_row))
        TEST_SELECT_BY_NAME_SQL = "select * from RENDEZVOUS where rdv_name = :rdv_name"
        ( the_only_row ), _ = datastore._select(TEST_SELECT_BY_NAME_SQL,{'rdv_name':DUMMY_NAME})
        self.assertEqual(1,len(the_only_row))
    def test_join_rdv(self):
        datastore = DataStore.create_db(_TEST_DB_NAME)
        ( rdv_id,ptcpt1_id,_ )=datastore._create_test_data()
        TEST_SELECT_BY_RDV_ID_SQL = "select * from PARTICIPANT where rdv_id = :rdv_id"
        rdv_id8 = datastore._id_base64_to_long(rdv_id)
        substitution_map = { PTCPT_COL_RDV_ID: rdv_id8 }
        ( two_rows ),_ = datastore._select(TEST_SELECT_BY_RDV_ID_SQL,substitution_map)
        self.assertEqual(2,len(two_rows))    
    def test_report_position(self):
        NORTHING,EASTING,TIME_T,RADIUS = 13.0,14.0,10000000,100
        datastore = DataStore.create_db(_TEST_DB_NAME)
        (rdv_id,ptcpt1_id,_)=datastore._create_test_data()
        datastore.report_position(rdv_id,ptcpt1_id,NORTHING,EASTING,TIME_T,RADIUS)
        ptcpt1_id8 = datastore._id_base64_to_long(ptcpt1_id)
        TEST_SELECT_BY_PTCPT_ID_SQL = "select * from PARTICIPANT where id = :id"
        substitution_map = { PTCPT_COL_ID:ptcpt1_id8 }
        ( one_row ), _ = datastore._select(TEST_SELECT_BY_PTCPT_ID_SQL,substitution_map)
        self.assertEqual(1,len(one_row)) 
        rdv_id8 = datastore._id_base64_to_long(rdv_id)
        self.assertEqual((ptcpt1_id8,rdv_id8),one_row[0][0:2])
    def test_list_rdv_ids(self):
        datastore = DataStore.create_db(_TEST_DB_NAME)
        (rdv_id,_,_)=datastore._create_test_data()
        ( one_id ) = datastore.list_rdv_ids()
        self.assertEqual(1,len(one_id))
        self.assertEqual(rdv_id, one_id[0])
    def test_get_rdv_details(self):
        NORTHING,EASTING,RADIUS,TIME_T = 13.0,14.0,100,10000000
        datastore = DataStore.create_db(_TEST_DB_NAME)
        (rdv_id,ptcpt1_id,ptcpt2_id)=datastore._create_test_data()
        datastore.report_position(rdv_id,ptcpt1_id,NORTHING,EASTING,RADIUS,TIME_T)
        rdv_details = datastore.get_rdv_details(rdv_id)
        self.assertEqual(rdv_id,rdv_details[0][0])
        self.assertEqual(rdv_details[1][0][0],ptcpt1_id)
        # self.assertEqual((NORTHING,EASTING,TIME_T,RADIUS),rdv_details[1][2][1:])

    # Methods intended for private use only
    def _cleanup(self) :
        try :
            os.remove(_TEST_DB_NAME)
        except FileNotFoundError :
            pass    

if __name__ == '__main__':
    unittest.main()
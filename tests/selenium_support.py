# selenium_support.py

import sys

# Selenium boilerplate from 
# http://www.seleniumhq.org/docs/03_webdriver.jsp#setting-up-a-selenium-webdriver-project
try :
    from selenium import webdriver
except :
    print("Selenium does not appear to be installed")
    print("Try 'pip.exe install selenium'")
    sys.exit(-99)
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
#from selenium.common.exceptions import TimeoutException

# The login/logout code below assumes that a single button is used for both signing in 
# and signing out, and that button is contained within the iframe with the 
# specified id at a node selected by the specified XPath.
SIGN_IN_OR_OUT_IFRAME_ID = "hdr_iframe"                       # could be None
SIGN_IN_OR_OUT_XPATH     = "//table/tbody/tr[2]/td[2]/button" # required

PARAM_URL_BASE = u'url_base'
PARAM_NORMAL_USERNAME = u'normal_username'
PARAM_NORMAL_PASSWORD = u'normal_password'
PARAM_ADMIN_USERNAME = u'admin_username'
PARAM_ADMIN_PASSWORD = u'admin_password'

class SeleniumDriverWrapper(object):
    def __init__(self, params):
        self.url_base = params[PARAM_URL_BASE]
        self.username = params[PARAM_NORMAL_USERNAME]
        self.password = params[PARAM_NORMAL_PASSWORD]
        self._create_driver_firefox()
    def dispose(self):
        self.driver.close()
        self.driver = None
    def login(self) :
        if(self.url_base.endswith(u'.appspot.com')) :
            self._google_login()
        else :
            self._simple_login()
    def logout(self):
        if(self.url_base.endswith(u'.appspot.com')) :
            self._google_logout()
        else :
            self._simple_logout()
    def get(self,url_suffix=None):
        if url_suffix :
            url =self.url_base + url_suffix
        else :
            url = self.url_base
        self.driver.get(url)
    def url(self): 
        return self.driver.current_url
    def title(self):
        return self.driver.title
    def element(self, xpath, frame_id=None):
        self.driver.switch_to.default_content()
        if frame_id is not None:
            self.driver.switch_to_frame(frame_id)
        # WebDriverWait(self.driver, 10).until(
        #    EC.presence_of_element_located((By.XPATH,xpath))
        #)
        return self.driver.find_element_by_xpath(xpath)
    def _simple_login(self):
        print("Attempting simple login")
        print("Login attempt completed")
    def _simple_logout(self):
        print("Attempting simple logout")
        print("Logout attempt completed")
    def _google_login(self):
        # Based on example code at:
        # https://gist.github.com/ikegami-yukino/51b247080976cb41fe93
        print("Attempting Google login")
        signin_button = self.element(SIGN_IN_OR_OUT_XPATH,SIGN_IN_OR_OUT_IFRAME_ID)
        signin_button.click()
        WebDriverWait(self.driver, 10).until(EC.title_contains("Sign in"))
        self.driver.find_element_by_id("Email").send_keys(self.username)
        self.driver.find_element_by_id("next").click()
        # The Google login page dynamically changes the id of the password
        # field to enable password entry.  Timing of this is not predictable, 
        # but if we wait for it to be found we will be OK.
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID,"Passwd"))
        )
        self.driver.find_element_by_id("Passwd").send_keys(self.password)
        self.driver.find_element_by_id("signIn").click()
        print("Login attempt completed")
    def _google_logout(self):
        print("Attempting Google logout")
        signout_button = self.element(SIGN_IN_OR_OUT_XPATH,SIGN_IN_OR_OUT_IFRAME_ID)
        signout_button.click()
        print("Logout attempt completed")
    def _create_driver_firefox(self):
        fp = webdriver.FirefoxProfile()
        fp.set_preference("browser.startup.homepage", "about:blank")
        fp.set_preference("startup.homepage_welcome_url", "about:blank")
        fp.set_preference("startup.homepage_welcome_url.additional", "about:blank")
        self.driver = webdriver.Firefox(fp)


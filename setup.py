from setuptools import setup

setup(name='OnMyWay',
      version='0.1',
      description='OpenShift App',
      author='Tim Littlefair',
      author_email='tim_littlefair@hotmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask','flask-wtf','flask-babel','markdown','flup','bcrypt'],
     )

@rem run.cmd
@rem Minimal script to run Kenneth Reitz's example program.
@echo off

cls

setlocal

rem Setup Python virtual environment
rem rmdir /s/q venv35
if not exist venv35 (
    py -3 -m venv venv35
    call ./venv35/Scripts/activate.bat
    rem The next line needs to use 'python -m pip' instead 
    rem of 'pip' because pip can't uninstall itself while 
    rem pip.exe is running
    python -m pip install --upgrade pip
    pip install flask
    pip install bcrypt
    pip install selenium
) else (
    call ./venv35/Scripts/activate.bat
)

rem Run unit tests on datastore
set PYTHONPATH=%CD%\on_my_way
python tests\all_tests.py
if errorlevel 1 (
	echo Tests failed 
	goto :errorEnd
) else (
	echo All tests passed
)

rem Run the web application
set PYTHONPATH=%CD%\on_my_way
rem cd on_my_way
start "WSGI server" cmd /c ".\venv35\Scripts\python.exe .\on_my_way\flask_runner.py && pause"
start http://127.0.0.1:5000

:errorEnd
endlocal
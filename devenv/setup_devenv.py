import io
import os.path
import urllib.request
import zipfile

def get_ezsetup(dir) :
    response_stream = urllib.request.urlopen("https://bootstrap.pypa.io/ez_setup.py")
    f = open(os.path.join(dir,"ez_setup.py"), 'wb')
    f.write( response_stream.read() )
    f.close()
    
def get_chromedriver(dir) :
    zip_stream = urllib.request.urlopen("http://chromedriver.storage.googleapis.com/2.21/chromedriver_win32.zip")
    zip_file = zipfile.ZipFile(io.BytesIO(zip_stream.read()))
    zip_file.extract("chromedriver.exe",dir)

target_dir = os.path.join("venv33","Scripts")
get_chromedriver(target_dir)
get_ezsetup(target_dir)

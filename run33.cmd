@rem run.cmd
@rem Minimal script to run Kenneth Reitz's example program.
@echo off

cls

setlocal

rem Setup Python virtual environment
rem rmdir /s/q venv33
if not exist venv33 (
	c:\python33\python c:\python33\tools\scripts\pyvenv.py venv33
    call .\venv33\Scripts\activate.bat
    python devenv\setup_devenv.py
    python .\venv33\Scripts\ez_setup.py
    easy_install pip
    pip install flask
    pip install bcrypt
    pip install selenium
) else (
    call .\venv33\Scripts\activate.bat
)
set PYTHONPATH=./on_my_way;./venv33/Lib/site_packages
rem Run unit tests on datastore
python tests\all_tests.py
if errorlevel 1 (
	echo Tests failed 
	goto :errorEnd
) else (
	echo All tests passed
)

rem Run the web application
rem cd on_my_way
rem start "WSGI server" cmd /c ".\venv33\Scripts\python.exe .\on_my_way\flask_runner.py && pause"
rem start http://localhost:5000

:errorEnd
endlocal
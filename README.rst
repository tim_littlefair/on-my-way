On My Way
=========

This is a simple web application based on Kenneth Reitz's recommendations
for Python project structure.

`Learn more <http://www.kennethreitz.org/essays/repository-structure-and-python>`_.

import os.path
import sqlite3
import time
import struct
import random
import base64

import model_entities

# A separate module has been created to contain the constants used for 
# the SQL used to access the datastore and the column names.
# This should be regarded as a private submodule under the current
# module and should never be accessed directly from anywhere else.
from _datastore_schema_constants import *
from _datastore_schema_constants import _RDV_TABLE_NAME, _RDV_TABLE_SQL, _RDV_CREATE_SQL, _RDV_DETAILS_SQL, _ALL_RDV_SQL
from _datastore_schema_constants import _PTCPT_TABLE_NAME, _PTCPT_TABLE_SQL, _PTCPT_JOIN_SQL, _PTCPT_DETAILS_SQL
from _datastore_schema_constants import _POSREP_TABLE_NAME, _POSREP_TABLE_SQL, _POSREP_INSERT_SQL, _POSREP_DETAILS_SQL

# Exception classes           
class DatabaseAlreadyExistsError(RuntimeError) :
    pass
class DatabaseDoesNotExistError(RuntimeError) :
    pass
class DatabaseTooFullError(RuntimeError):
    pass

class DataStore :
    @staticmethod
    def create_db(filepath,prime_with_test_data=False) :
        if os.path.exists(filepath) :
            raise DatabaseAlreadyExistsError
        retval = DataStore(filepath)
        retval._create_schema()
        if prime_with_test_data :
            retval._create_test_data()
        return retval
    
    @staticmethod
    def set_db_filepath(filepath) :
        if not os.path.exists(filepath) :
            raise DatabaseDoesNotExistError
        return DataStore(filepath)
    
    def __init__(self,filepath):
        self.filepath = filepath
    
    def open_db(self):
        self.cxn = sqlite3.connect(self.filepath)

    def close_db(self):
        self.cxn.commit()
        self.cxn.close()
        self.cxn = None
    
    def create_rdv(self,rdv_name,meet_time_t=None,duration_hours=None):
        if duration_hours is None :
            hours_before,hours_after = (1.0,)*2
        else :
            hours_before,hours_after = (duration_hours/2.0,)*2
        if meet_time_t is None :
            meet_time_t = time.time() + 60*60*hours_before
        substitution_map = {
            RDV_COL_NAME : rdv_name,
            RDV_COL_CREATE_TIME : time.time(),
            RDV_COL_MEET_TIME : meet_time_t,
            RDV_COL_HOURS_BEFORE : hours_before,
            RDV_COL_HOURS_AFTER : hours_after 
        }
        rdv_id8=self._insert(_RDV_CREATE_SQL,substitution_map)
        return self._id_long_to_base64(rdv_id8)
    
    def join_rdv(self,rdv_id,ptcpt_name):
        rdv_id8 = self._id_base64_to_long(rdv_id)
        substitution_map = {
            PTCPT_COL_RDV_ID : rdv_id8,
            PTCPT_COL_NAME : ptcpt_name,
        }
        ptcpt_id8 = self._insert(_PTCPT_JOIN_SQL,substitution_map)
        return self._id_long_to_base64(ptcpt_id8)
    
    def report_position(self, rdv_id, ptcpt_id, northing, easting, radius,timestamp=None):
        ptcpt_id8 = self._id_base64_to_long(ptcpt_id)
        if timestamp is None :
            timestamp = time.time()
        substitution_map = {
            POSREP_COL_PTCPT_ID : ptcpt_id8,
            POSREP_COL_NORTHING : northing,
            POSREP_COL_EASTING : easting,
            POSREP_COL_TIMESTAMP : timestamp,
            POSREP_COL_RADIUS : radius
        }
        self._insert(_POSREP_INSERT_SQL,substitution_map)
    
    def list_rdv_ids(self):
        id_row_list,_ = self._select(_ALL_RDV_SQL)
        return [ self._id_long_to_base64(row[0]) for row in id_row_list ]  
    
    def get_rdv_details(self,rdv_id):
        rdv_id8 = self._id_base64_to_long(rdv_id)
        substitution_map = { RDV_COL_ID: rdv_id8 }
        ( rdv_details,),_ = self._select(_RDV_DETAILS_SQL,substitution_map)
        substitution_map = { PTCPT_COL_RDV_ID: rdv_id8 }
        ptcpt_attrs_list,_ = self._select(_PTCPT_DETAILS_SQL,substitution_map)
        ptcpt_list = []
        for ptcpt_attrs in ptcpt_attrs_list :
            substitution_map = { POSREP_COL_PTCPT_ID : ptcpt_attrs[0] }
            ptcpt_posrep_list,_ = self._select(_POSREP_DETAILS_SQL,substitution_map)
            ptcpt_posrep_list = [self._replace_id(posrep,1) for posrep in ptcpt_posrep_list ]
            ptcpt_list += [ ptcpt_attrs + ( ptcpt_posrep_list, ) ]
        return ( 
            self._replace_id(rdv_details),
            [ self._replace_id(ptcpt_details,1) for ptcpt_details in ptcpt_list ]
        )
        
    # Methods which are intended for internal/test use only
    def _create_schema(self):
        self.open_db()
        self.cxn.execute(_RDV_TABLE_SQL)
        self.cxn.execute(_PTCPT_TABLE_SQL)
        self.cxn.execute(_POSREP_TABLE_SQL)
        self.close_db()
    def _create_test_data(self,rdv_details_list=None) :
        retval = []
        if rdv_details_list is None :
            rdv_details_list = [ model_entities._test_rdv_object() ]
        for rdv in rdv_details_list :
            rdv.id = self.create_rdv(rdv.name,rdv.meet_time,rdv.hours_before+rdv.hours_after)
            retval += [rdv.id]
            for ptcpt in rdv.participants :
                ptcpt.id=self.join_rdv(rdv.id,ptcpt.name)
                retval += [ptcpt.id]
                for posrep in ptcpt.position_reports :
                    self.report_position(
                        rdv.id, ptcpt.id,posrep.northing,posrep.easting,
                        posrep.radius_metres,posrep.timestamp
                )
        return retval
    def _insert(self,sql,substitution_map={}):
        self.open_db()
        for _ in range(0,3) :
            try :
                # The rowid must fit into a signed integer so
                # we could use up to 63 bits.
                # 48 works better as it encodes using base64
                # to 8 bytes.
                substitution_map['random_id']=random.getrandbits(48)
                cursor = self.cxn.execute(sql,substitution_map)
                break
            except sqlite3.IntegrityError:
                # After 3 attempts we have failed to find a 
                # rowid not already in use
                raise DatabaseTooFullError()
        lastrowid = cursor.lastrowid
        self.close_db()
        return lastrowid
    def _update(self,sql,substitution_map={}):
        self.open_db()
        cursor = self.cxn.execute(sql,substitution_map)
        rowcount = cursor.rowcount          
        self.close_db()
        return rowcount
    def _select(self,sql,substitution_map={}):
        self.open_db()
        cursor = self.cxn.execute(sql,substitution_map)
        rows = cursor.fetchall()
        column_info = [ c[0] for c in cursor.description ]
        self.close_db()
        return rows, column_info
        
    # The next two methods convert between 64-bit numeric
    # ids (as used by Sqlite) and 11-character string ids 
    # which are shorter for inclusion in URLs.
    
    def _id_long_to_base64(self,id_long) :
        id8_bytes = bytes(struct.unpack("8B",struct.pack("Q",id_long)))
        id6_bytes = id8_bytes[0:6]
        return str(base64.urlsafe_b64encode(id6_bytes),'ascii')
        
    def _id_base64_to_long(self,id_base64) :
        id6_bytes = list(base64.urlsafe_b64decode(id_base64))
        id8_bytes = bytes(id6_bytes + [0,0])
        return struct.unpack("Q",id8_bytes)[0]
    
    def _replace_id(self,attr_tuple,skip_elements=0):
        retval = []
        retval += [ self._id_long_to_base64(attr_tuple[0])]
        retval += attr_tuple[skip_elements+1:] 
        return tuple(retval)
        
        
        
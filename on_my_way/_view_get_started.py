# _view_get_started.py

# Private submodule under views.py to handle the get_started button
# on the default page.

import flask

def _get_started(style) :
    params=flask.request.args
    return flask.render_template(
        'create.html',style=style,params=params
    )

# _view_index.py

# Private submodule under views.py to handle the default Page

import flask

def _index(style):
    return flask.render_template('index.html',style=style)

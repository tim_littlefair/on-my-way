# _datastore_schema_constants.py

# This script contains the constant strings used to define 
# and access the datastore's SQL schema.
# It should be treated as a private submodule of datastore.py
# and should not be directly imported by any other module.

_RDV_TABLE_NAME = "RENDEZVOUS"
_RDV_TABLE_SQL = """
    create table RENDEZVOUS (
        id INTEGER PRIMARY KEY,
        rdv_name TEXT,
        create_time_t INTEGER,
        meet_time_t INTEGER,
        hours_before REAL,
        hours_after REAL
    );
"""

RDV_COL_ID = "id"
RDV_COL_NAME = "rdv_name"
RDV_COL_CREATE_TIME = "create_time_t"
RDV_COL_MEET_TIME = "meet_time_t"
RDV_COL_HOURS_BEFORE = "hours_before"
RDV_COL_HOURS_AFTER = "hours_after"
RDV_NUM_COLUMNS = 6

_PTCPT_TABLE_NAME = "PARTICIPANT"
_PTCPT_TABLE_SQL = """
    create table PARTICIPANT (
        id INTEGER PRIMARY KEY,
        rdv_id INTEGER,
        ptcpt_name TEXT,
        ptcpt_seq INTEGER
    )
"""

PTCPT_COL_ID = 'id'
PTCPT_COL_RDV_ID = 'rdv_id'
PTCPT_COL_SEQ = 'ptcpt_seq'
PTCPT_COL_NAME = 'ptcpt_name'
PTCPT_NUM_COLUMNS=4

_POSREP_TABLE_NAME = "POSITION_REPORT"
_POSREP_TABLE_SQL = """
    create table POSITION_REPORT (
        id INTEGER PRIMARY KEY,
        ptcpt_id INTEGER,
        northing REAL,
        easting REAL,
        timestamp INTEGER,
        radius_metres REAL
    )
"""

POSREP_COL_ID = 'id'
POSREP_COL_PTCPT_ID = 'ptcpt_id'
POSREP_COL_NORTHING = 'northing'
POSREP_COL_EASTING = 'easting'
POSREP_COL_TIMESTAMP = 'timestamp'
POSREP_COL_RADIUS = 'radius_metres'
POSREP_NUM_COLUMNS = 7

_BEGIN_TXN = "begin transaction"
_COMMIT_TXN = "commit transaction"

_RDV_CREATE_SQL = """
    insert into RENDEZVOUS values (
        :random_id,
        :rdv_name, :create_time_t, :meet_time_t, 
        :hours_before,:hours_after
    )
"""

_PTCPT_JOIN_SQL = """
    insert into PARTICIPANT 
    select 
        :random_id,
        :rdv_id, :ptcpt_name, count(*)
    from PARTICIPANT
    where rdv_id = :rdv_id
"""

_POSREP_INSERT_SQL = """
    insert into POSITION_REPORT values (
        :random_id,
        :ptcpt_id,
        :northing,
        :easting,
        :timestamp,
        :radius_metres
    )
"""

_ALL_RDV_SQL = """
    select id from RENDEZVOUS
    order by create_time_t
"""

_RDV_DETAILS_SQL = """
    select * from RENDEZVOUS
    where id = :id
"""

_PTCPT_DETAILS_SQL = """
    select * from PARTICIPANT
    where rdv_id = :rdv_id
    order by ptcpt_seq
"""

_POSREP_DETAILS_SQL = """
    select * from POSITION_REPORT
    where ptcpt_id = :ptcpt_id
    order by timestamp
"""

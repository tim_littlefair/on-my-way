# _view_parse_datetime.py

# Script to recover the datetime based on parameters provided by 
# a web form.
# Different browsers encode the value differently, this function
# is intended to provide best effort at matching this.

import sys
import logging
import time

def _parse_datetime(datetime_string, time_t_string, tz_string, user_agent):
    LOCALTIME_FORMATS = ("%Y-%m-%dT%H:%M",)
    time_t_dt_parsed = None
    for format in LOCALTIME_FORMATS :
        try :
            time_t_dt_parsed = time.mktime(time.strptime(datetime_string,format))
            break
        except :
            logging.error("dt_string="+datetime_string)
            logging.error("exception_message="+str(sys.exc_info()[1]))
            logging.error("user_agent="+str(user_agent))
            pass
    try : 
        time_t_int = int(time_t_string)
    except :
        time_t_int = int(time_t_dt_parsed)
    return time_t_int + 60*int(tz_string) 
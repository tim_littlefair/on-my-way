# _view_style.py

# private submodule under views.py for handling style selection

def _select_style(user_agent,logger=None):
    if logger :
        logger.info(user_agent.platform)
        logger.info(user_agent.string)
    if "Windows NT" in user_agent.string :
        return 'desktop_laptop'
    else :
        return 'phone'

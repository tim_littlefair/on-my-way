# model_entities.py
 
# This module defines entity classes which are exported by the model module.
# They have been separated into a separate module so that an instance
# of the rendezvous entity class can be used by the datastore module for 
# priming test data without creating circular dependencies.
 
import datetime

class DetailsBase :
    def __repr__(self):
        return self.__dict__.__repr__()
 
class PositionReportDetails(DetailsBase) :
    def __init__(self,*args):
        ( self.id,
            self.northing, self.easting,
            self.timestamp,self.radius_metres 
        ) = args

class ParticipantDetails(DetailsBase) :
    def __init__(self,*args):
        ( self.id, self.name, self.seq, ) = args[0:3]
        self.position_reports = []
        try :
            for posrep in args[3] :
                self.position_reports += [ PositionReportDetails(*posrep) ]
        except IndexError :
            # Optional parameter not present
            pass

class RendezvousDetails(DetailsBase) :
    def __init__(self,*args):
        ( 
            self.id, self.name, self.create_time, 
            self.meet_time, self.hours_before, self.hours_after 
        ) = args[0:6]
        self.participants = []
        try :
            for ptcpt in args[6] :
                try :
                    self.participants += [ ParticipantDetails(*ptcpt) ]
                except TypeError:
                    self.participants += [ ptcpt ]
        except IndexError :
            # Optional parameter not present
            pass  
        
def _test_rdv_object() :
    base_datetime = datetime.datetime.strptime(
            '20160713T190000Z+0000','%Y%m%dT%H%M%SZ%z'
    )
    base_time_t = base_datetime.timestamp()                            
    rdv_details = RendezvousDetails(
        'test-rdv-id','Shopping',base_time_t,base_time_t,1.0,1.0,
        (
            ('test-ptcpt1-id','Alice',0,(
                # She was at Picadilly Circus 
                (
                    None,51.5101, -0.1342, 
                    50,float(base_time_t-600)
                ),
                # Then at Leicester Square
                (
                    None,51.5105, -0.1301, 
                    50,float(base_time_t-300)
                ),
            )),
            ('test-ptcpt2-id','Bob',1,(
                # He was at Charing Cross 
                (
                    None,51.5073, -0.1276, 
                    120,base_time_t-200.0
                ),
            )),
        )
    )
    return rdv_details

#"""
if __name__ == "__main__" :
    test_obj = _test_rdv_object()
    print(test_obj)
#"""    

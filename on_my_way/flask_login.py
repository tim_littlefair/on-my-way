# flask_login.py

# Login capability based on:
# http://flask.pocoo.org/snippets/8/

from functools import wraps
import flask

from stored_pw_hash import PW_HASH
import hash_password

def check_auth(username, password):
    return hash_password.verify_hash(username,password,PW_HASH)

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return flask.Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = flask.request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated
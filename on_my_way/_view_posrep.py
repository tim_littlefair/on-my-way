# _view_get_started.py

# Private submodule under views.py to serve the position reporting 
# form and handle its submission.

import flask

from model import Model

def _posrep(style) :
    if flask.request.method=='GET' :
        params = flask.request.args
        return flask.render_template('posrep.html',params=params,style=style)
    else :
        _the_model = Model()
        params=flask.request.form
        rdv_id=params['rdv_id']
        ptcpt_id=params['ptcpt_id']
        northing=params['northing']
        easting=params['easting']
        radius=params['radius_metres']
        _the_model.report_position(rdv_id,ptcpt_id,northing,easting,radius)
        redirect_url = '/rendezvous?rdv_id=%s&ptcpt_id=%s'%(rdv_id,ptcpt_id,)
        return flask.redirect(redirect_url,303)

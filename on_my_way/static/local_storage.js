var user_name=document.getElementById('user_name')
var current_ids=document.getElementById('current_ids')
var future_ids=document.getElementById('future_ids')

function loadElementFromLocalStorage(element,localStorageValue,defaultValue)
{
	if(element==null)
	{
		// Do nothing.
	}
	else if(localStorageValue!=null) 
	{
		element.value=localStorageValue;
	}
	else
	{
		element.value=defaultValue
	}
}

function loadFromLocalStorage()
{
	EMPTY_LIST = ""
	loadElementFromLocalStorage(user_name,localStorage.user_name,'anonymous')
	loadElementFromLocalStorage(current_ids,localStorage.currentIds,EMPTY_LIST)
}
function saveToLocalStorage(user_name,current_ids)
{
	localStorage.user_name=user_name
	localStorage.current_ids=current_ids
}

loadFromLocalStorage()

# _view_get_started.py

# Private submodule under views.py to serve the map display  
# page and handle the position updates it sends

import time
import flask

from model import Model

def _map(style) :
    _TIME_FORMAT = "%d %b %Y %H:%M:%S"
    if flask.request.method=='GET' :
        params=flask.request.args
        _the_model = Model()
        rdv_details = _the_model.get_rdv_details(params['rdv_id'])
        markers = []
        for participant in rdv_details.participants :
            for position_report in participant.position_reports :
                markers += [ 
                    ( 
                        position_report.northing, position_report.easting,
                        '%s at %s' % (
                            participant.name,time.strftime(
                                _TIME_FORMAT,time.localtime(position_report.timestamp)
                            )
                        )
                    )
                ]
        return flask.render_template(
            'rendezvous.html',
            rdv_details=rdv_details,markers=markers,
            params=params,
            style=style
        )
    else :
        _the_model = Model()
        params=flask.request.form
        rdv_id=params['rdv_id']
        ptcpt_id=params['ptcpt_id']
        northing=params['northing']
        easting=params['easting']
        radius=params['radius_metres']
        _the_model.report_position(rdv_id,ptcpt_id,northing,easting,radius)
        redirect_url = '/rendezvous?rdv_id=%s&ptcpt_id=%s'%(rdv_id,ptcpt_id,)
        return flask.redirect(redirect_url,303)

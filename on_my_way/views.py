# views.py

import logging
import time

import flask

import flask_login
from model import Model

from _view_style import _select_style
from _view_index import _index
from _view_get_started import _get_started
from _view_posrep import _posrep
from _view_map import _map
from _view_parse_datetime import _parse_datetime

app = flask.Flask(__name__,
    static_folder='static',template_folder='templates') 
app.config['PROPAGATE_EXCEPTIONS'] = True 
app.logger.setLevel(logging.INFO)

@app.route('/')
@app.route('/index')
def index():
    style=_select_style(flask.request.user_agent,app.logger)
    return _index(style)

@app.route('/get_started',methods=['GET','POST'])
def get_started():
    style=_select_style(flask.request.user_agent,app.logger)
    return _get_started(style)
    
@app.route('/posrep', methods=['GET','POST'])
def posrep():
    style=_select_style(flask.request.user_agent,app.logger)
    return _posrep(style)

@app.route('/reset')
@flask_login.requires_auth
def reset() :
    Model.reset(prime_with_test_data=True)
    return overview()

@app.route('/overview')
@flask_login.requires_auth
def overview() :
    _the_model = Model()
    all_rdv_details = [ 
        _the_model.get_rdv_details(rdv_id)
        for rdv_id in _the_model.list_rdv_ids()
    ]   
    return flask.render_template('overview.html',
        all_rdv_details=all_rdv_details,
        len=len, time=time)

@app.route('/rendezvous',methods=['GET','POST'])
def rendezvous() :
    style=_select_style(flask.request.user_agent,app.logger)
    return _map(style)

@app.route('/create', methods=['POST'])
def create():
    _the_model = Model()
    params=flask.request.form
    user_name=params['user_name']
    rdv_name=params['rdv_name']
    meet_time_t = _parse_datetime(
        params['meet_dt'],params['meet_time_t'],params['tz_offset'],
        flask.request.user_agent
    )
    duration_hours = float(params['duration_hours'])
    rdv_id = _the_model.create_rdv(rdv_name,meet_time_t,duration_hours)
    ptcpt_id = _the_model.join_rdv(rdv_id,user_name)
    if None is None :
        northing = float(params['northing'])
        easting = float(params['easting'])
        radius_metres = float(params['radius_metres'])
        _the_model.report_position(rdv_id,ptcpt_id,northing,easting,radius_metres)
        redirect_url = '/rendezvous?rdv_id=%s&ptcpt_id=%s'%(
            rdv_id,ptcpt_id,
        )
        return flask.redirect(redirect_url,303)
    else :
        pass

@app.route('/join', methods=['GET','POST'])
def join():
    if flask.request.method=='GET' :
        params=flask.request.args
        return flask.render_template('join.html',params=params)
    else :
        _the_model = Model()
        params=flask.request.form
        rdv_id=params['rdv_id']
        ptcpt_name=params['ptcpt_name']
        ptcpt_id =_the_model.join_rdv(rdv_id,ptcpt_name)
        redirect_url = '/posrep?rdv_id=%s&ptcpt_id=%s'%(rdv_id,ptcpt_id,)
        return flask.redirect(redirect_url,303)


@app.route('/shutdown', methods=['GET','POST'])
def shutdown():
    func = flask.request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

# model.py

import os.path
import time

from datastore import DataStore, DatabaseDoesNotExistError
from model_entities import ParticipantDetails, RendezvousDetails

_db_name=None
_the_datastore = None
_the_model = None

    
class Model:   
    @staticmethod 
    def set_db_name(name="model.db"):
        global _db_name 
        try :
            _db_name = os.path.join(os.environ['OPENSHIFT_DATA_DIR'],name)
        except KeyError:
            _db_name = name
    @staticmethod 
    def reset(prime_with_test_data=False) :
        global _the_model
        global _the_datastore
        if _the_model :
            _the_model.close_ds()
        if(os.path.exists(_db_name)) :
            os.remove(_db_name)
        _the_datastore = DataStore.create_db(_db_name,prime_with_test_data)
        if _the_model :
            _the_model.datastore = _the_datastore
    def __init__(self):
        global _the_model
        global _the_datastore
        self.datastore = None
        _the_model = self
        try :
            if _the_datastore is None :
                _the_datastore = DataStore.set_db_filepath(_db_name)
        except DatabaseDoesNotExistError :
            Model.reset(False)
        self.datastore = _the_datastore
    
    def create_rdv(self,rdv_name,meet_time_t,duration_hours):
        return self.datastore.create_rdv(rdv_name,meet_time_t,duration_hours)
    
    def join_rdv(self,rdv_id,ptcpt_name):
        return self.datastore.join_rdv(rdv_id, ptcpt_name)
    
    def report_position(self,rdv_id,ptcpt_id,northing,easting,radius):
        return self.datastore.report_position(rdv_id,ptcpt_id,northing,easting,radius)
    
    def list_rdv_ids(self):
        return self.datastore.list_rdv_ids()
    
    def get_rdv_details(self,rdv_id):
        rdv_attrs,ptcpt_list = self.datastore.get_rdv_details(rdv_id)
        rdv = RendezvousDetails(
            rdv_attrs[0],rdv_attrs[1],rdv_attrs[2],
            rdv_attrs[3],rdv_attrs[4],rdv_attrs[5],
            [
                ParticipantDetails(
                    ptcpt_attr[0],ptcpt_attr[1],ptcpt_attr[2],ptcpt_attr[3]
                ) for ptcpt_attr in ptcpt_list
            ]
        )
        return rdv
    
    def close_ds(self):
        if self.datastore is not None :
            self.datastore=None
        
 
Model.set_db_name()

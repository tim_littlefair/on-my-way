# wsgi.py
# Alternative version of the Hello World app described in
# https://blog.openshift.com/how-to-install-and-configure-a-python-flask-dev-environment-deploy-to-openshift/

# This version uses the code snippets from page above but packages them
# as single wsgi.py file instead of files run.py, wsgi\__init__.py and wsgi\views.py

from views import app

if __name__ == "__main__" :  
    # configure_logging()
    app.run(
            debug=True,
            host='0.0.0.0'
    )
    